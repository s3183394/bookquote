package nl.utwente.di.bookQuote;

import java.util.Queue;
import org.junit.Test;
import org.junit.jupiter.api.Assertions;



public class TestQuoter {

    @Test
    public void testBook1() throws Exception{
        Quoter quoter = new Quoter();
        double price = quoter.getBookPrice("1");
        Assertions.assertEquals(10.0, price, 0.0, "Priceof book 1 ");
    }
}
